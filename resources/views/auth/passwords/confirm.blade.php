@extends('layouts.app')
@section('title', 'Konfirmasi Kata Sandi')

@section('content')
<div class='py-5 px-3'>
    <h1>{{ __('Konfirmasi Kata Sandi') }}</h1>
    <form method="POST" action="{{ route('password.confirm') }}">
        @csrf
        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Katasandi') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Konfirmasi Kata Sandi') }}
                </button>

                @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Kamu Lupa Kata Sandi?') }}
                    </a>
                @endif
            </div>
        </div>
    </form>
</div>
@endsection
