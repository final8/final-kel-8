<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $artikels = Artikel::with('post')->get();
        // foreach ($questions as $question) {
        //     $title = $question->title;
        //     $content = $question->post->content;
        //     $user = $question->post->user->name;
        //     $created = $question->post->created_at->diffForHumans(null, true) . ' ago';
        //     $total = $question->count();
        // }
        return view('index', compact('artikels'));
    }
    public function awal()
    {
        $artikels = Artikel::with('post')->get();
        // foreach ($questions as $question) {
        //     $title = $question->title;
        //     $content = $question->post->content;
        //     $user = $question->post->user->name;
        //     $created = $question->post->created_at->diffForHumans(null, true) . ' ago';
        //     $total = $question->count();
        // }
        return view('home', compact('artikels'));
    }
}
