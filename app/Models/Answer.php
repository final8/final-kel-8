<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $guarded = [];

    public function post()
    {
        return $this->hasOne('App\Models\Post', 'id', 'post_id');
    }
}
